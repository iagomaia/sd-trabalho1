/*
    Trabalho 1 de Sistemas Distribuídos
    Iago Maia
    Jhenmilly Keyth
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <math.h>

pthread_mutex_t soma_value_lock = PTHREAD_MUTEX_INITIALIZER;
int soma_global;
int values_per_thread;
int *vetor;
int num_values;
int pthread_yield (void);

void *soma(void *list_ptr){
    int *partial_list_ptr, i;
    partial_list_ptr = (int *)list_ptr;

    int id=pthread_self();
    for (i = 0; i < values_per_thread; i++){
		printf("Tread %d entrou na região crítica\n", id);
        pthread_mutex_lock(&soma_value_lock);
        soma_global += partial_list_ptr[i];
		printf("Soma parcial: %d\n", soma_global);
        pthread_mutex_unlock(&soma_value_lock);
		printf("Tread %d saiu da região crítica\n", id);
		pthread_yield();
    }
    pthread_exit(0);
}


int main(int argc, char *argv[]) {
    // inicializa mutex

    pthread_mutex_init(&soma_value_lock, NULL);
    num_values = atoi(argv[1]);
    vetor = malloc(num_values*sizeof(int));
    int i = 0;
    for(i = 0; i < num_values; i++){
        vetor[i] = (rand()%100);
		printf("Valor %d: %d\n", i, vetor[i]);
    }
    int num_threads = atoi(argv[2]);
    values_per_thread = num_values / num_threads;
	printf("Valores por thread: %d\n", values_per_thread);

    // cria threads
	int t, result;
	char err_msg[128];
    int partial_result = 0;
    pthread_t thread[num_threads];

	for(t = 0; t < num_threads; t++) {
		if((partial_result=pthread_create(&thread[t], NULL, soma, (void *)(vetor + (t * values_per_thread))))) {
			strerror_r(result,err_msg,128);
			printf("Erro criando thread: %s\n",err_msg);
			exit(0);
		}
	}

	// espera threads conclu�rem
	for(t=0; t<num_threads; t++) {
		if((result=pthread_join(thread[t], NULL))) {
			strerror_r(result,err_msg,128);
			printf("Erro em pthread_join: %s\n",err_msg);
			exit(0);
		}
	}

	printf("Resultado: %d\n", soma_global);

	pthread_mutex_destroy (&soma_value_lock);
	pthread_exit(NULL);
}